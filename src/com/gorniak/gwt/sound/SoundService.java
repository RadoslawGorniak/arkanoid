package com.gorniak.gwt.sound;

import com.google.gwt.dom.client.AudioElement;
import com.google.gwt.media.client.Audio;
import com.gorniak.gwt.resources.SoundResources;

public final class SoundService {

    private static Audio loseGameAudio;
    private static Audio winGameAudio;
    private static Audio pickupBallAudio;
    private SoundResources soundResources;

    public SoundService(SoundResources soundResources) {
        this.soundResources = soundResources;
        loadLoseGameAudio();
        loadWinGameAudio();
        loadPickupBallAudio();
    }

    private void loadLoseGameAudio() {
        loseGameAudio = Audio.createIfSupported();
        if (loseGameAudio != null) {
            loseGameAudio.addSource(soundResources.soundLoseGame().getSafeUri().asString(), AudioElement.TYPE_WAV);
            loseGameAudio.load();
        }
    }

    private void loadWinGameAudio() {
        winGameAudio = Audio.createIfSupported();
        if (winGameAudio != null) {
            winGameAudio.addSource(soundResources.soundWinGame().getSafeUri().asString(), AudioElement.TYPE_WAV);
            winGameAudio.load();
        }
    }

    private void loadPickupBallAudio() {
        pickupBallAudio = Audio.createIfSupported();
        if (pickupBallAudio != null) {
            pickupBallAudio.addSource(soundResources.soundPickupBall().getSafeUri().asString(), AudioElement.TYPE_WAV);
            pickupBallAudio.load();
        }
    }

    public static Audio getLoseGameAudio() {
        return loseGameAudio;
    }

    public static void playLoseGameAudio(){
        loseGameAudio.play();
    }

    public static Audio getWinGameAudio() {
        return winGameAudio;
    }

    public static void playWinGameAudio(){
        winGameAudio.play();
    }

    public static Audio getPickupBallAudio() {
        return pickupBallAudio;
    }

    public static void playPickupBallAudio(){
        pickupBallAudio.play();
    }
}

