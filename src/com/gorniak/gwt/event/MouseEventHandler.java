package com.gorniak.gwt.event;

import com.google.gwt.canvas.client.Canvas;
import com.gorniak.gwt.components.paddle.Paddle;

public final class MouseEventHandler {

    private MouseEventHandler() {
    }

    public static void initial(Canvas canvas) {
        canvas.addMouseMoveHandler(event -> {
            setPosition(event.getX());
        });
        canvas.addTouchMoveHandler(event -> {
            setPosition(event.getTouches().get(0).getClientX());
        });
    }

    private static void setPosition(int posX) {
        Paddle paddle = Paddle.getInstance();
        paddle.getPosition().setPosX((posX - paddle.getSize().getWidth() / 2));
    }
}
