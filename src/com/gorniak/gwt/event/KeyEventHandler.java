package com.gorniak.gwt.event;

import com.google.gwt.canvas.client.Canvas;
import com.gorniak.gwt.components.ball.Ball;
import com.gorniak.gwt.components.paddle.Paddle;

import static com.gorniak.gwt.resources.GameResources.*;

public final class KeyEventHandler {

    private KeyEventHandler() {
    }

    public static void initial(Canvas canvas) {
        canvas.addKeyDownHandler(
                event -> {
                    setKey(event.getNativeKeyCode(), true);
                });
        canvas.addKeyUpHandler(
                event -> {
                    setKey(event.getNativeKeyCode(), false);
                });
    }

    private static void setKey(int key, boolean keyValue) {
        Ball ball = Ball.getInstance();
        switch (key) {
            case KEY_LEFT:
                Paddle.getInstance().setKeyLeft(keyValue);
                break;
            case KEY_UP:
                if (ball.getBallSpeed() < 5)
                    ball.setBallSpeed(ball.getBallSpeed() + 1);
                break;
            case KEY_RIGHT:
                Paddle.getInstance().setKeyRight(keyValue);
                break;
            case KEY_DOWN:
                if (ball.getBallSpeed() >= 1) ball.setBallSpeed(ball.getBallSpeed() - 1);
                break;
            default:
                break;
        }
    }
}
