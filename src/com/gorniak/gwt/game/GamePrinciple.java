package com.gorniak.gwt.game;

import com.gorniak.gwt.client.ArkanoidGameUser;
import com.gorniak.gwt.resources.GraphicResources;
import com.gorniak.gwt.components.ball.Ball;
import com.gorniak.gwt.components.block.Block;
import com.gorniak.gwt.components.paddle.Paddle;
import com.gorniak.gwt.sound.SoundService;

import static com.gorniak.gwt.resources.GameResources.*;


public final class GamePrinciple {

    private static ArkanoidGameUser arkanoidGameUser = ArkanoidGameUser.getInstance();

    private GamePrinciple() {
    }

    public static void principles() {
        gameEndTime();
        gameEndLife();
        gameWin();
    }

    private static void gameEndTime() {
        arkanoidGameUser.setGameTime((int) (GAME_TIME - (System.currentTimeMillis() - TIME_CURRENTLY) / 1000));
        if (arkanoidGameUser.getGameTime() == 0) {
            arkanoidGameUser.setInGame(false);
            SoundService.playLoseGameAudio();
        }
    }

    private static void gameEndLife() {
        Ball ball = Ball.getInstance();
        if (ball.getPosition().getPosY() + ball.getSize().getHeight() >= HEIGHT_GAME - 1 && arkanoidGameUser.getGameLife() > 0) {
            arkanoidGameUser.setGameLife(arkanoidGameUser.getGameLife() - 1);
            restartStatsComponents();
        } else if (ball.getPosition().getPosY() + ball.getSize().getHeight() >= HEIGHT_GAME - 1) {
            arkanoidGameUser.setInGame(false);
            SoundService.playLoseGameAudio();
        }
    }

    private static void gameWin() {
        Block[] blocks = arkanoidGameUser.getBlocks();
        for (int i = 0, j = 0; i < N_OF_BLOCKS; i++) {
            if (!(blocks[i].getBlockColorEnum().getBlockLife() > 0))
                j++;
            if (j == N_OF_BLOCKS) {
                arkanoidGameUser.setGraphicEndGame(GraphicResources.INSTANCE.winnerGraphic());
                arkanoidGameUser.setInGame(false);
                SoundService.playWinGameAudio();
            }
        }
    }

    private static void restartStatsComponents() {
        Ball ball = Ball.getInstance();
        Paddle paddle = Paddle.getInstance();
        ball.getPosition().setPosX(INIT_POSITION_X_BALL);
        ball.getPosition().setPosY(INIT_POSITION_Y_BALL);
        ball.setDirX(INIT_DIR_X_BALL);
        ball.setDirY(INIT_DIR_Y_BALL);
        ball.setBallSpeed(INIT_LEVEL_FAST_BALL);
        paddle.getPosition().setPosX(INIT_POSITION_X_PADDLE);
        paddle.getPosition().setPosY(INIT_POSITION_Y_PADDLE);
        paddle.setDirX(ZERO);
    }
}
