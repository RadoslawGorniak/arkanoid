package com.gorniak.gwt.game;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;
import com.gorniak.gwt.client.ArkanoidGameUser;
import com.gorniak.gwt.resources.GraphicResources;
import com.gorniak.gwt.components.ball.Ball;
import com.gorniak.gwt.components.block.Block;
import com.gorniak.gwt.components.paddle.Paddle;
import com.gorniak.gwt.components.utils.Component;
import com.gorniak.gwt.components.utils.Position;

import static com.gorniak.gwt.resources.GameResources.*;

public final class GameRender {

    private Context2d context;

    public GameRender(Canvas canvas) {
        this.context = canvas.getContext2d();
    }

    public void render() {
        if (!ArkanoidGameUser.getInstance().isInGame()) {
            drawImage(ImageElement.as(new Image(ArkanoidGameUser.getInstance().getGraphicEndGame()).getElement()),100, 220);
        } else {
            clearBoard();
            drawComponents();
            drawLife();
            drawText(String.valueOf(ArkanoidGameUser.getInstance().getGameTime()), new Position(WIDTH_GAME - 33, 18));
        }
    }

    private void clearBoard() {
        context.clearRect(0, 0, WIDTH_GAME, HEIGHT_GAME);
    }

    public void drawLife() {
        Image image = new Image(GraphicResources.INSTANCE.lifeGraphic());
        ImageElement imageElement = ImageElement.as(image.getElement());
        int i = 0;
        while (i < ArkanoidGameUser.getInstance().getGameLife()) {
            drawImage(imageElement, 55 + (30 * i), 1);
            i++;
        }
    }

    public void drawComponents() {
        drawComponent(Paddle.getInstance());
        drawComponent(Ball.getInstance());
        drawBlocks(ArkanoidGameUser.getInstance().getBlocks());
    }

    public void drawText(String text, Position position) {
        context.setTextAlign(Context2d.TextAlign.LEFT);
        context.setTextBaseline(Context2d.TextBaseline.MIDDLE);
        context.setFont(FONT_SETTINGS);
        context.setFillStyle(FONT_COLOR);
        context.fillText(text, position.getPosX(), position.getPosY());
    }

    private void drawComponent(Component component) {
        drawImage(component.getImage(), component.getPosition().getPosX(), component.getPosition().getPosY());
    }

    private void drawImage(ImageElement imageElement, int posX, int posY) {
        context.drawImage(imageElement, posX, posY);
    }

    private void drawBlocks(Block[] blocks) {
        for (Block block : blocks)
            if (block.getBlockColorEnum().getBlockLife() > 0)
                drawComponent(block);
    }
}
