package com.gorniak.gwt.game;

import com.gorniak.gwt.client.ArkanoidGameUser;
import com.gorniak.gwt.components.ball.BallCollision;
import com.gorniak.gwt.components.ball.BallCollisionBlock;
import com.gorniak.gwt.components.ball.BallCollisionBorder;
import com.gorniak.gwt.components.ball.BallCollisionPaddle;

public class GameCollision {

    public GameCollision() {}

    public void initialCollision() {

        setCollisionBallAndBlock();
        setCollisionBallAndBorder();
        setCollisionBallAndPaddle();
    }

    private void setCollisionBallAndPaddle() {
        checkCollision(new BallCollisionPaddle());
    }

    private void setCollisionBallAndBorder() {
        checkCollision(new BallCollisionBorder());
    }

    private void setCollisionBallAndBlock() {
        checkCollision(new BallCollisionBlock(ArkanoidGameUser.getInstance().getBlocks()));
    }

    private void checkCollision(BallCollision collision) {
        collision.collision();
    }
}
