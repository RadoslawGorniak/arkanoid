package com.gorniak.gwt.client;

import com.google.gwt.animation.client.AnimationScheduler;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.gorniak.gwt.event.KeyEventHandler;
import com.gorniak.gwt.event.MouseEventHandler;
import com.gorniak.gwt.game.GameCollision;
import com.gorniak.gwt.game.GamePrinciple;
import com.gorniak.gwt.game.GameRender;
import com.gorniak.gwt.resources.SoundResources;
import com.gorniak.gwt.components.ball.Ball;
import com.gorniak.gwt.components.paddle.Paddle;
import com.gorniak.gwt.components.utils.InteractionMove;
import com.gorniak.gwt.sound.SoundService;

import java.util.Arrays;
import java.util.List;

import static com.gorniak.gwt.resources.GameResources.DIV_TAG_ID;
import static com.gorniak.gwt.resources.GameResources.FPS_GAME;

public final class ArkanoidGame implements EntryPoint {

    private List<InteractionMove> gameComponentsMove;
    GameCollision gameCollision = new GameCollision();
    private GameRender gameRender;
    private SoundService soundService;
    private Canvas canvas;

    public void onModuleLoad() {
        gameComponentsMove = Arrays.asList(Ball.getInstance(), Paddle.getInstance());
        canvas = CanvasCreator.createCanvas();
        soundService = new SoundService(SoundResources.INSTANCE);
        gameRender = new GameRender(canvas);
        RootPanel.get(DIV_TAG_ID).add(canvas);
        KeyEventHandler.initial(canvas);
        MouseEventHandler.initial(canvas);
        runGame();
    }

    private void runGame() {
        AnimationScheduler.get().requestAnimationFrame(new AnimationScheduler.AnimationCallback() {
            @Override
            public void execute(double v) {
                update();
                render();
                AnimationScheduler.get().requestAnimationFrame(v1 -> execute(FPS_GAME), canvas.getCanvasElement());
            }
        }, canvas.getCanvasElement());
    }

    private void update() {
        GamePrinciple.principles();
        gameComponentsMove.forEach(InteractionMove::move);
        gameCollision.initialCollision();
    }

    private void render() {
        gameRender.render();
    }
}