package com.gorniak.gwt.client;


import com.google.gwt.resources.client.ImageResource;
import com.gorniak.gwt.resources.GraphicResources;
import com.gorniak.gwt.components.block.Block;
import com.gorniak.gwt.components.block.BlockSchemaGeneratorLevel;
import com.gorniak.gwt.components.block.BlockSchemaGeneratorLevel1;

import java.io.Serializable;


import static com.gorniak.gwt.resources.GameResources.*;

public final class ArkanoidGameUser implements Serializable {
    private static final long serialVersionUID = 23232323;

    private int gameLife;
    private int gameTime;
    private boolean inGame;
    private ImageResource graphicEndGame;
    private Block[] blocks;
    private static ArkanoidGameUser INSTANCE = new ArkanoidGameUser();

    private ArkanoidGameUser() {
        this.gameLife = GAME_LIFE;
        this.gameTime = GAME_TIME;
        this.inGame = true;
        this.graphicEndGame = GraphicResources.INSTANCE.gameOverGraphic();
        chanceBlockSchema(new BlockSchemaGeneratorLevel1());
    }

    public static ArkanoidGameUser getInstance(){
        return INSTANCE;
    }

    public int getGameLife() {
        return gameLife;
    }

    public void setGameLife(int gameLife) {
        this.gameLife = gameLife;
    }

    public int getGameTime() {
        return gameTime;
    }

    public void setGameTime(int gameTime) {
        this.gameTime = gameTime;
    }

    public boolean isInGame() {
        return inGame;
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }

    public Block[] getBlocks() {
        return blocks;
    }

    public ImageResource getGraphicEndGame() {
        return graphicEndGame;
    }

    public void setGraphicEndGame(ImageResource graphicEndGame) {
        this.graphicEndGame = graphicEndGame;
    }

    public void chanceBlockSchema(BlockSchemaGeneratorLevel blockSchemaGeneratorLevel){
        this.blocks = blockSchemaGeneratorLevel.generateLevel();
    }
}
