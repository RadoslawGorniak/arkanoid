package com.gorniak.gwt.client;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

import static com.gorniak.gwt.resources.GameResources.*;

public final class CanvasCreator{

    private CanvasCreator() {}

    public static Canvas createCanvas() {
        Canvas canvas = Canvas.createIfSupported();
        if (canvas == null) {
            RootPanel.get().add(new Label(DOESN_T_SUPPORT_THE_HTML_5_CANVAS_ELEMENT));
        }
        assert canvas != null;
        canvas.setStyleName(STYLE_NAME_CANVAS);
        canvas.setWidth(WIDTH_GAME + PX);
        canvas.setCoordinateSpaceWidth(WIDTH_GAME);
        canvas.setHeight(HEIGHT_GAME + PX);
        canvas.setCoordinateSpaceHeight(HEIGHT_GAME);
        canvas.setFocus(true);
        canvas.getCanvasElement().getStyle().setBackgroundImage(BACKGROUND_IMAGE);
        return canvas;
    }
}
