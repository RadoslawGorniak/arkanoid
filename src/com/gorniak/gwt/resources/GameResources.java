package com.gorniak.gwt.resources;

public class GameResources {

    private GameResources(){
    }
    public static final int WIDTH_GAME = 600;
    public static final int HEIGHT_GAME = 600;
    public static final String DIV_TAG_ID = "canvasExample";
    public static final int N_OF_BLOCKS = 24;
    public static final int N_OF_ROW_BLOCKS = N_OF_BLOCKS / 8;
    public static final int N_OF_COLUMN_BLOCKS = N_OF_BLOCKS / 3;
    public static final int INIT_POSITION_X_PADDLE = 200;
    public static final int INIT_POSITION_Y_PADDLE = 560;
    public static final int INIT_POSITION_X_BALL = 230;
    public static final int INIT_POSITION_Y_BALL = 535;
    public static final int INIT_LEVEL_FAST_BALL = 0;
    public static final int PADDLE_WIDTH = 124;
    public static final int PADDLE_HEIGHT = 25;
    public static final int BLOCK_WIDTH = 60;
    public static final int BLOCK_HEIGHT = 30;
    public static final int BALL_WIDTH = 24;
    public static final int BALL_HEIGHT = 24;
    public static final int KEY_LEFT = 37;
    public static final int KEY_UP = 38;
    public static final int KEY_RIGHT = 39;
    public static final int KEY_DOWN = 40;
    public static final double INIT_DIR_X_BALL = 2;
    public static final double INIT_DIR_Y_BALL = -2;
    public static final double DIR_Y = 2;
    public static final int FPS_GAME = 16;
    public static final int BLOCK_BLUE_LIFE = 2;
    public static final int BLOCK_RED_LIFE = 3;
    public static final int BLOCK_YELLOW_LIFE = 1;
    public static final int ONE = 1;
    public static final int ZERO = 0;
    public static final int GAME_LIFE = 3;
    public static final int GAME_TIME = 120;
    public static final double MS_PER_UPDATE = 100;
    public static final long TIME_CURRENTLY =  System.currentTimeMillis();
    public static final int LEVEL_GAME_FAST = 1;
    public static final String FONT_SETTINGS = "bold 18px sans-serif";
    public static final String FONT_COLOR = "#FFF";
    public static final String DOESN_T_SUPPORT_THE_HTML_5_CANVAS_ELEMENT = "Sorry, your browser doesn't support the HTML5 Canvas element";
    public static final String BACKGROUND_IMAGE = "url('" + GraphicResources.INSTANCE.boardArkanoidGraphic().getSafeUri().asString() + "')";
    public static final String PX = "px";
    public static final String STYLE_NAME_CANVAS = "mainCanvas";
}
