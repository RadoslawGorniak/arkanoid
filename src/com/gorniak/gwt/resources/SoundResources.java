package com.gorniak.gwt.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;

public interface SoundResources extends ClientBundle {

    SoundResources INSTANCE =  GWT.create(SoundResources.class);

    @Source("com/gorniak/gwt/resources/music/win_game.wav")
    DataResource soundWinGame();

    @Source("com/gorniak/gwt/resources/music/lose_game.wav")
    DataResource soundLoseGame();

    @Source("com/gorniak/gwt/resources/music/pickup.wav")
    DataResource soundPickupBall();
}
