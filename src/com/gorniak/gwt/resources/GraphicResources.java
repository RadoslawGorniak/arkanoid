package com.gorniak.gwt.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface GraphicResources extends ClientBundle {

    public static final GraphicResources INSTANCE = GWT.create(GraphicResources.class);

    @Source("com/gorniak/gwt/resources/graphic/ball.png")
    ImageResource ballGraphic();

    @Source("com/gorniak/gwt/resources/graphic/paddle.png")
    ImageResource paddleGraphic();

    @Source("com/gorniak/gwt/resources/graphic/blue_block.png")
    ImageResource blueBlockGraphic();

    @Source("com/gorniak/gwt/resources/graphic/red_block.png")
    ImageResource redBlockGraphic();

    @Source("com/gorniak/gwt/resources/graphic/yellow_block.png")
    ImageResource yellowBlockGraphic();

    @Source("com/gorniak/gwt/resources/graphic/winner.png")
    ImageResource winnerGraphic();

    @Source("com/gorniak/gwt/resources/graphic/game_over.png")
    ImageResource gameOverGraphic();

    @Source("com/gorniak/gwt/resources/graphic/board_arkanoid.jpg")
    ImageResource boardArkanoidGraphic();

    @Source("com/gorniak/gwt/resources/graphic/life.png")
    ImageResource lifeGraphic();

}
