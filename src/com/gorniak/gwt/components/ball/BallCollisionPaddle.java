package com.gorniak.gwt.components.ball;

import com.gorniak.gwt.components.paddle.Paddle;
import com.gorniak.gwt.components.utils.Position;
import com.gorniak.gwt.sound.SoundService;

import static com.gorniak.gwt.resources.GameResources.INIT_DIR_X_BALL;
import static com.gorniak.gwt.resources.GameResources.INIT_DIR_Y_BALL;

public class BallCollisionPaddle implements BallCollision {
    private Paddle paddle;
    private Ball ball;

    public BallCollisionPaddle() {
        this.ball = Ball.getInstance();
        this.paddle = Paddle.getInstance();
    }

    @Override
    public void collision() {
        Position ballPosition = ball.getPosition();
        Position paddlePosition = paddle.getPosition();
        if ((ballPosition.getPosX() + ball.getSize().getWidth() >= paddlePosition.getPosX())
                && (ballPosition.getPosX() <= paddlePosition.getPosX() + paddle.getSize().getWidth())
                && (ballPosition.getPosY() + ball.getSize().getHeight() >= paddlePosition.getPosY())
                && (ballPosition.getPosY() <= paddlePosition.getPosY() + paddle.getSize().getHeight())) {
           SoundService.playPickupBallAudio();

            if ((ballPosition.getPosX() - paddlePosition.getPosX() >= -ball.getSize().getWidth()) && (ballPosition.getPosX() - paddlePosition.getPosX() <= paddle.getSize().getWidth() / 2)) {
                if (ball.getDirX() < 0)
                    ball.setDirX(-addBallSpeed(INIT_DIR_X_BALL + 1.5));
                else
                    ball.setDirX(addBallSpeed(INIT_DIR_X_BALL));
            } else if ((ballPosition.getPosX() - paddlePosition.getPosX() > paddle.getSize().getWidth() - paddle.getSize().getWidth() / 2)
                    && (ballPosition.getPosX() - paddlePosition.getPosX() <= paddle.getSize().getWidth())) {
                if (ball.getDirX() < 0)
                    ball.setDirX(-addBallSpeed(INIT_DIR_X_BALL));
                else
                    ball.setDirX(addBallSpeed(INIT_DIR_X_BALL + 1.5));
            }
            ball.setDirY(-addBallSpeed(-INIT_DIR_Y_BALL));
        }
    }

    private double addBallSpeed(double dir) {
        return dir + ball.getBallSpeed();
    }
}
