package com.gorniak.gwt.components.ball;


import com.gorniak.gwt.components.block.Block;
import com.gorniak.gwt.components.block.BlockImageChanging;
import com.gorniak.gwt.components.utils.Position;
import com.gorniak.gwt.sound.SoundService;

import static com.gorniak.gwt.resources.GameResources.N_OF_BLOCKS;


public class BallCollisionBlock implements BallCollision {

    private Ball ball;
    private Block[] blocks;
    private BlockImageChanging blockImageChanging;

    public BallCollisionBlock(Block[] blocks) {
        this.blockImageChanging = new BlockImageChanging();
        this.ball = Ball.getInstance();
        this.blocks = blocks;
    }

    @Override
    public void collision() {
        Position ballPosition = ball.getPosition();
        for (int i = 0; i < N_OF_BLOCKS; i++) {
            Position blockPosition = blocks[i].getPosition();
            int blockLastLife = blocks[i].getBlockColorEnum().getBlockLife();

            if ((ballPosition.getPosX() + ball.getSize().getWidth() >= blockPosition.getPosX())
                    && (ballPosition.getPosX() <= blockPosition.getPosX() + blocks[i].getSize().getWidth())
                    && (ballPosition.getPosY() + ball.getSize().getHeight() >= blockPosition.getPosY())
                    && (ballPosition.getPosY() <= blockPosition.getPosY() + blocks[i].getSize().getHeight())
                    && blockLastLife > 0) {
                SoundService.playPickupBallAudio();
                ball.setDirY(-ball.getDirY());
                ball.setDirX(-ball.getDirX());
                blockImageChanging.chanceImage(blocks[i]);
                break;
            }
        }
    }
}