package com.gorniak.gwt.components.ball;

import com.gorniak.gwt.components.utils.Component;
import com.gorniak.gwt.resources.GraphicResources;
import com.gorniak.gwt.components.utils.InteractionMove;
import com.gorniak.gwt.components.utils.Position;
import com.gorniak.gwt.components.utils.Size;

import java.io.Serializable;

import static com.gorniak.gwt.resources.GameResources.*;


public class Ball extends Component implements Serializable, InteractionMove {

    private static final long serialVersionUID = 23232323;
    private double dirX;
    private double dirY;
    private int ballSpeed;

    private static Ball INSTANCE = new Ball();

    public Ball() {
        super(new Position(INIT_POSITION_X_BALL, INIT_POSITION_Y_BALL),
                GraphicResources.INSTANCE.ballGraphic(),
                new Size(BALL_WIDTH, BALL_HEIGHT));
        this.dirX = INIT_DIR_X_BALL;
        this.dirY = INIT_DIR_Y_BALL;
        this.ballSpeed = INIT_LEVEL_FAST_BALL;
    }

    public static Ball getInstance() {
        return INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    public double getDirX() {
        return dirX;
    }

    public void setDirX(double dirX) {
        this.dirX = dirX;
    }

    public double getDirY() {
        return dirY;
    }

    public void setDirY(double dirY) {
        this.dirY = dirY;
    }

    public int getBallSpeed() {
        return ballSpeed;
    }

    public void setBallSpeed(int ballSpeed) {
        this.ballSpeed = ballSpeed;
        if(getDirX() < 0) setDirX(-(INIT_DIR_X_BALL+ getBallSpeed()));
        else setDirX(INIT_DIR_X_BALL+ getBallSpeed());
        if(getDirY() < 0) setDirY(-(DIR_Y+ getBallSpeed()));
        else setDirY(DIR_Y+ getBallSpeed());
    }

    @Override
    public void move() {
        getPosition().chancePositionAddedMove(dirX, dirY);
        if (getPosition().getPosX() == ZERO) {
            setDirX(-getDirX());
        }
        if (getPosition().getPosX() == WIDTH_GAME - getSize().getWidth()) {
            setDirX(-getDirX());
        }
    }

}
