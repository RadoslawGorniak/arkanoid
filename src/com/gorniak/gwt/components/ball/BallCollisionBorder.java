package com.gorniak.gwt.components.ball;


import com.gorniak.gwt.components.utils.Position;

import static com.gorniak.gwt.resources.GameResources.*;


public class BallCollisionBorder implements BallCollision{

    private Ball ball;

    public BallCollisionBorder() {
        this.ball = Ball.getInstance();
    }

    @Override
    public void collision() {
        Position ballPosition = ball.getPosition();
        if (ballPosition.getPosX() + ball.getSize().getWidth() >= WIDTH_GAME-1) {
            ball.setDirX(-ball.getDirX());

        }
        if (ballPosition.getPosX() <= 1) {
            ball.setDirX(-ball.getDirX());

        }
        if (ballPosition.getPosY() <= 1) {
            ball.setDirY(-ball.getDirY());

        }
    }
}
