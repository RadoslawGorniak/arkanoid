package com.gorniak.gwt.components.paddle;


import com.gorniak.gwt.components.utils.Component;
import com.gorniak.gwt.resources.GraphicResources;
import com.gorniak.gwt.components.utils.InteractionMove;
import com.gorniak.gwt.components.utils.Position;
import com.gorniak.gwt.components.utils.Size;

import java.io.Serializable;

import static com.gorniak.gwt.resources.GameResources.*;


public final class Paddle extends Component implements Serializable, InteractionMove {

    private static final long serialVersionUID = 23232323;
    private int dirX;
    private static Paddle INSTANCE = new Paddle();
    private boolean keyLeft;
    private boolean keyRight;

    private Paddle() {
        super(new Position(INIT_POSITION_X_PADDLE, INIT_POSITION_Y_PADDLE),
                GraphicResources.INSTANCE.paddleGraphic(),
                new Size(PADDLE_WIDTH, PADDLE_HEIGHT));
        this.dirX = ZERO;
        this.keyLeft = false;
        this.keyRight = false;
    }

    public static Paddle getInstance() {
        return INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    public int getDirX() {
        return dirX;
    }

    public void setDirX(int dirX) {
        this.dirX = dirX;
    }

    public boolean isKeyLeft() {
        return keyLeft;
    }

    public void setKeyLeft(boolean keyLeft) {
        this.keyLeft = keyLeft;
    }

    public boolean isKeyRight() {
        return keyRight;
    }

    public void setKeyRight(boolean keyRight) {
        this.keyRight = keyRight;
    }

    @Override
    public void move() {
        getPosition().setPosX(getPosition().getPosX() + dirX);
        if (getPosition().getPosX() < ZERO)
            getPosition().setPosX(ZERO);
        if (getPosition().getPosX() >= WIDTH_GAME - getSize().getWidth())
            getPosition().setPosX(WIDTH_GAME - getSize().getWidth());
        if ((keyLeft) && getPosition().getPosX() > 0)
            getPosition().setPosX(getPosition().getPosX() - 10);
        if ((keyRight) && (getPosition().getPosX() + getSize().getWidth() < WIDTH_GAME))
            getPosition().setPosX(getPosition().getPosX() + 10);
    }

}
