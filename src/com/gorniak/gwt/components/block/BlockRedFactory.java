package com.gorniak.gwt.components.block;


import com.gorniak.gwt.components.utils.Position;

public class BlockRedFactory extends BlockFactory {

    @Override
    public Block createBlock(Position position) {
        return new Block(position, BlockColorEnum.RED);
    }
}
