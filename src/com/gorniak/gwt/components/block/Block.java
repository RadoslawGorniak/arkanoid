package com.gorniak.gwt.components.block;


import com.gorniak.gwt.components.utils.Component;
import com.gorniak.gwt.components.utils.Position;
import com.gorniak.gwt.components.utils.Size;

import static com.gorniak.gwt.resources.GameResources.BLOCK_HEIGHT;
import static com.gorniak.gwt.resources.GameResources.BLOCK_WIDTH;

public class Block extends Component {

    private BlockColorEnum blockColorEnum;

    public Block(Position position, BlockColorEnum blockColorEnum) {
        super(position, blockColorEnum.getImageResourceBlock(), new Size(BLOCK_WIDTH, BLOCK_HEIGHT));
        this.blockColorEnum = blockColorEnum;
    }

    public BlockColorEnum getBlockColorEnum() {
        return blockColorEnum;
    }

    public void setBlockColorEnum(BlockColorEnum blockColorEnum) {
        this.blockColorEnum = blockColorEnum;
    }
}
