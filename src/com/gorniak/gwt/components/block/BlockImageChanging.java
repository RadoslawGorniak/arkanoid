package com.gorniak.gwt.components.block;


import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;

public class BlockImageChanging {

    public void chanceImage(Block block) {
        int blockLife = block.getBlockColorEnum().getBlockLife();
        if (blockLife - 1 >= 0) {
            block.setBlockColorEnum(BlockColorEnum.getBlockColorEnum(blockLife - 1));
            Image image = new Image(block.getBlockColorEnum().getImageResourceBlock());
            block.setImage(ImageElement.as(image.getElement()));
        }
    }
}
