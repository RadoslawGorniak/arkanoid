package com.gorniak.gwt.components.block;


import com.gorniak.gwt.components.utils.Position;

import static com.gorniak.gwt.resources.GameResources.*;


public class BlockSchemaGeneratorLevel1 implements BlockSchemaGeneratorLevel{

    private static Position position;

    @Override
    public Block[] generateLevel() {
        Block[] blocks = new Block[N_OF_BLOCKS];
        int k = ZERO;
        for (int i = ZERO; i < N_OF_ROW_BLOCKS; i++) {
            for (int j = ZERO; j < N_OF_COLUMN_BLOCKS; j++) {
                position = new Position(j * 70 + 15, i * 40 + 50);
                blocks[k] = createBlockWithPosition(i + 1);
                k++;
            }
        }
        return blocks;
    }

    private static Block createBlockWithPosition(int i) {
        switch (i) {
            case 3: {
                return createBlock(new BlockYellowFactory());
            }
            case 2: {
                return createBlock(new BlockBlueFactory());
            }
            case 1: {
                return createBlock(new BlockRedFactory());
            }
            default:
                throw new IllegalArgumentException();
        }
    }

    private static Block createBlock(BlockFactory factory) {
        return factory.createBlock(position);
    }
}
