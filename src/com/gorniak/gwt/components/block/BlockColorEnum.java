package com.gorniak.gwt.components.block;


import com.google.gwt.resources.client.ImageResource;
import com.gorniak.gwt.resources.GraphicResources;

import static com.gorniak.gwt.resources.GameResources.*;

public enum BlockColorEnum {
    YELLOW(GraphicResources.INSTANCE.yellowBlockGraphic(), BLOCK_YELLOW_LIFE),
    BLUE(GraphicResources.INSTANCE.blueBlockGraphic(), BLOCK_BLUE_LIFE),
    RED(GraphicResources.INSTANCE.redBlockGraphic(), BLOCK_RED_LIFE),
    DEFAULT(GraphicResources.INSTANCE.yellowBlockGraphic(),0);

    private ImageResource imageResourceBlock;
    private int blockLife;

    BlockColorEnum(ImageResource imageResourceBlock, int blockLife) {
        this.imageResourceBlock = imageResourceBlock;
        this.blockLife = blockLife;
    }

    public ImageResource getImageResourceBlock() {
        return imageResourceBlock;
    }

    public int getBlockLife() {
        return blockLife;
    }

    public static BlockColorEnum getBlockColorEnum(int blockLife){
        switch (blockLife){
            case 1: return YELLOW;
            case 2: return BLUE;
            case 3: return RED;
            default: return DEFAULT;
        }
    }
}
