package com.gorniak.gwt.components.block;


import com.gorniak.gwt.components.utils.Position;

public abstract class BlockFactory {
    public abstract Block createBlock(Position position);
}
