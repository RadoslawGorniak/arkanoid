package com.gorniak.gwt.components.block;

public interface BlockSchemaGeneratorLevel {
    Block[] generateLevel();
}
