package com.gorniak.gwt.components.utils;

import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;

public abstract class Component {

    private Position position;
    private Size size;
    private ImageElement image;

    public Component(Position position, ImageResource imageResource, Size size) {
        this.position = position;
        this.image = ImageElement.as(new Image(imageResource).getElement());
        this.size = size;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public ImageElement getImage() {
        return image;
    }

    public void setImage(ImageElement image) {
        this.image = image;
    }
}
